# Diffusive_solver (version 3.0)

Package for solving systems of coupled diffusion equations in a device setup using Finite
Element Method (FEM).
The package is based on [FeniCS](https://fenicsproject.org/) FEM project.

A rigorous formulation of the mathematical problem and a description of the basic functionalities can be found in https://arxiv.org/abs/2011.04351.

## Installation

Diffusive_solver requires FEniCS. Please refer to the FEniCS [installation
page](https://fenicsproject.org/download/).

After installing FEniCS diffusive_solver can be obtained via pip

    pip install diffusive-solver

or downloaded from the project [repository](https://gitlab.com/itorre/diffusive_solver).

## Examples

Please refer to the project [repository](https://gitlab.com/itorre/diffusive_solver/tests) for example notebooks.

## License

The package diffusive_solver is distributed under the GNU Lesser General Public License v3 (LGPLv3).

## Cite as

If you use diffusive_solver for your research please cite

[I. Torre "*Diffusive solver: a diffusion-equations solver based
on FEniCS*" 	arXiv:2011.04351 (2020).](https://arxiv.org/abs/2011.04351)