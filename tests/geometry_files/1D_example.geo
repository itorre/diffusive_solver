SetFactory("OpenCASCADE");

lc = .1;


Point(1) = { 0 , 0, 0, lc };
Point(2) = { 4, 0,0,lc *2};

Line(1) = {1,2};


Physical Line('sample', 3) = {1};
Physical Point('source', 1) = {1};
Physical Point('drain', 2) = {2};