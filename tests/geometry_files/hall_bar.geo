SetFactory("OpenCASCADE");

lc = .1;


Point(1) = { 0 , 0, 0, lc };
Point(2) = { 4, 0,0,lc};
Point(3) = { 4, 1,0,lc};
Point(4) = { 0, 1,0,lc};
Point(5) = {1, 0.75, 0, lc};
Point(6) = {1, 0.25, 0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Line(5) = {5,6};

Line Loop(6) = {1,2,3,4};
Plane Surface(7) = {6};
Line{5} In Surface {7};

Physical Surface('sample', 0) = {7};
Physical Curve('source', 1) = {2};
Physical Curve('drain', 2) = {4};
Physical Curve('wall', 3) = {5};